from utils import load_my_data, extract_DenseSurf_descriptors, load_batik_data
from utils import extract_DenseSift_descriptors
from utils import build_codebook
from utils import input_vector_encoder
from classifier import svm_classifier
import spm

import numpy as np
import argparse
import cPickle

parser = argparse.ArgumentParser(description='Image loader')
parser.add_argument("--train", type=str, help='load training data')
parser.add_argument("--test", type=str, help='load test data (optional)')

args = parser.parse_args()
print "Training data load from {}".format(args.train)
print "Test data load from {}".format(args.test)

x_train, y_train = load_batik_data(args.train)
x_test, y_test = load_batik_data(args.test)

# to save time...
# X_tr = X_train[:200]
# y_tr = y_train[:200]
# X_te = X_test[:200]
# y_te = y_test[:200]

VOC_SIZE = 400
PYRAMID_LEVEL = 0

print "Train/Test split: {:d}/{:d}".format(len(y_train), len(y_test))
print "Codebook Size: {:d}".format(VOC_SIZE)
print "Pyramid level: {:d}".format(PYRAMID_LEVEL)

print("SURF feature extraction")
# x_train = [extract_surf_descriptors(img) for img in x_train]
# x_test = [extract_surf_descriptors(img) for img in x_test]

x_train = [extract_DenseSurf_descriptors(img) for img in x_train]
x_test = [extract_DenseSurf_descriptors(img) for img in x_test]

x_train = [each for each in zip(x_train, y_train) if each[0] is not None]
x_train, y_train = zip(*x_train)
x_test = [each for each in zip(x_test, y_test) if each[0] is not None]
x_test, y_test = zip(*x_test)

print("Building the codebook, it will take some time")
codebook = build_codebook(x_train, voc_size=VOC_SIZE)

with open('./bow_codebook.pkl', 'w') as f:
    cPickle.dump(codebook, f)

print "Spatial Pyramid Matching encoding"
x_train = [spm.spatial_pyramid_matching(x_train[i], y_train[i], codebook, level=PYRAMID_LEVEL) for i in
           xrange(len(x_train))]
x_test = [spm.spatial_pyramid_matching(x_test[i], y_test[i], codebook, level=PYRAMID_LEVEL) for i in
          xrange(len(x_test))]

print("Bag of words encoding")
# x_train = [input_vector_encoder(x, codebook) for x in x_train]
x_train = np.asarray(x_train)

# x_test = [input_vector_encoder(each, codebook) for each in x_test]
x_test = np.asarray(x_test)

svm_classifier(x_train, y_train, x_test, y_test)
