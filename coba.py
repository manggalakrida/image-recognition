import numpy as np

if __name__ == '__main__':

    accuracy_result = open('accuracy-result-with_C=0.1-5.txt', 'wt')
    for c in np.arange(0.1, 5, 0.1):
        accuracy_result.write("C = " + str(c) + "\n")
        print ("C = ", c)
    accuracy_result.close()