C =1.0, Accuracy:90.4 %
              precision    recall  f1-score   support

      ceplok       0.75      0.75      0.75        16
      kawung       1.00      0.96      0.98        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.90      0.89      0.89       125
weighted avg       0.91      0.90      0.90       125
C =1.1, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =1.2000000000000002, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =1.3000000000000003, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =1.4000000000000004, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =1.5000000000000004, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =1.6000000000000005, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =1.7000000000000006, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =1.8000000000000007, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =1.9000000000000008, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =2.000000000000001, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =2.100000000000001, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =2.200000000000001, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =2.300000000000001, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =2.4000000000000012, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =2.5000000000000013, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =2.6000000000000014, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =2.7000000000000015, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =2.8000000000000016, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =2.9000000000000017, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
C =3.0000000000000018, Accuracy:89.60000000000001 %
              precision    recall  f1-score   support

      ceplok       0.71      0.75      0.73        16
      kawung       1.00      0.91      0.95        23
       nitik       0.95      0.78      0.86        23
      parang       0.89      0.96      0.93        53
       semen       0.91      1.00      0.95        10

   micro avg       0.90      0.90      0.90       125
   macro avg       0.89      0.88      0.88       125
weighted avg       0.90      0.90      0.90       125
