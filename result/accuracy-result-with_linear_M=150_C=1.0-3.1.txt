C =1.0, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =1.1, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =1.2000000000000002, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =1.3000000000000003, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =1.4000000000000004, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =1.5000000000000004, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =1.6000000000000005, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =1.7000000000000006, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =1.8000000000000007, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =1.9000000000000008, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =2.000000000000001, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =2.100000000000001, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =2.200000000000001, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =2.300000000000001, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =2.4000000000000012, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =2.5000000000000013, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =2.6000000000000014, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =2.7000000000000015, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =2.8000000000000016, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =2.9000000000000017, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
C =3.0000000000000018, Accuracy:84.8 %
              precision    recall  f1-score   support

      ceplok       0.57      0.75      0.65        16
      kawung       0.92      1.00      0.96        23
       nitik       0.90      0.78      0.84        23
      parang       0.94      0.89      0.91        53
       semen       0.67      0.60      0.63        10

   micro avg       0.85      0.85      0.85       125
   macro avg       0.80      0.80      0.80       125
weighted avg       0.86      0.85      0.85       125
