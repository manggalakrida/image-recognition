from sklearn import preprocessing
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV

import extraction as ex
from classifier import get_label
from utils import load_my_data, extract_surf_descriptors, extract_DenseSurf_descriptors
import numpy as np
import argparse
from sklearn import svm


parser = argparse.ArgumentParser(description='Image loader')
parser.add_argument("--train", type=str, help='load training data')
parser.add_argument("--test", type=str, help='load test data (optional)')

args = parser.parse_args()
print "Training data load from {}".format(args.train)
print "Test data load from {}".format(args.test)

train_data, train_label = load_my_data(args.train)
test_data, test_label = load_my_data(args.test)

# x_train = [extract_DenseSurf_descriptors(img) for img in train_data]
# x_test = [extract_DenseSurf_descriptors(img) for img in test_data]

x_train = [extract_surf_descriptors(img) for img in train_data]
x_test = [extract_surf_descriptors(img) for img in test_data]

all_train_desc = []
for i in range(len(x_train)):
    for j in range(x_train[i].shape[0]):
        all_train_desc.append(x_train[i][j, :])

all_train_desc = np.array(all_train_desc)

# =============== SURF + BoF + kNN ====================== #

# k = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50]
# scores = []
# for i in range(len(k)):
#     kmeans = ex.clusterFeatures(all_train_desc, k[i])
#     print "Clustering done !"
#     train_hist = ex.formTrainingSetHistogram(x_train, kmeans, k[i])
#     print "Build histogram done!"
#
#     # preprocess training histograms
#     scaler = preprocessing.StandardScaler().fit(train_hist)
#     train_hist = scaler.transform(train_hist)
#     print "Preprocessing histogram done !"
#
#     predict = ex.predictKMeans(kmeans, scaler, x_test, train_hist, train_label, test_label, k[i])
#     res = ex.accuracy(predict, test_label)
#     scores.append(res)
#     print("k =", k[i], ", Accuracy:", res * 100, "%")
#
# plt.figure()
# plt.title("SURF + BoF + kNN")
# plt.xlabel('k(n)')
# plt.ylabel('accuracy(x100%)')
# plt.plot(k, scores, '-r')
# plt.xticks([0, 100, 150, 200, 250, 300, 350, 400, 450, 500])
# plt.xticks([0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50])
# plt.show()

# =============== SURF + BoF + SVM ====================== #

k = 60
# L = 1
# scores = []
kmeans = ex.clusterFeatures(all_train_desc, k)
print "Clustering done !"

train_hist = ex.formTrainingSetHistogram(x_train, kmeans, k)
test_hist = ex.formTrainingSetHistogram(x_test, kmeans, k)
# train_hist = ex.getHistogramSPM(L, train_data, kmeans, k)
# test_hist = ex.getHistogramSPM(L, test_data, kmeans, k)
print "Build histogram done!"

scaler = preprocessing.StandardScaler().fit(train_hist)
train_hist = scaler.transform(train_hist)
test_hist = scaler.transform(test_hist)
print "Preprocessing histogram done !"

# c = 1.0
# clf = svm.SVC(random_state=0, kernel='rbf')
# clf.fit(train_hist, train_label)
# predict = clf.predict(test_hist)
# true = test_label
# # accuracy_result.write("C =" + str(c) + ", Accuracy:" + str(np.mean(predict == test_label) * 100) + " %" + "\n")
# print ("C =", c, ",\t Accuracy:", np.mean(predict == test_label) * 100, "%")
# print(classification_report(true, predict, target_names=get_label()))

# best_parameter = open('result/good-paramater-spm-with-M=100-L=1.txt', 'wt')
#
# C_range = 10.0 ** np.arange(-3, 3)
# gamma_range = 10.0 ** np.arange(-3, 3)
# param_grid = dict(gamma=gamma_range.tolist(), C=C_range.tolist())
#
# # Grid search for C, gamma
# print("Tuning hyper-parameters\n")
# clf = GridSearchCV(svm.SVC(), param_grid, cv='warn', n_jobs=-2)
# clf.fit(train_hist, train_label)
# print("Best parameters set found on development set:\n")
# print(clf.best_estimator_)
# best_parameter.write("Best parameters set found on development set:\n" + str(clf.best_estimator_))
# best_parameter.close()

# Train one-vs-all SVMs using sklearn
accuracy_result = open('pengujian/accuracy-result-without_spm_rbf_gamma=0.001_M=60_C=1.0-11.0.txt', 'wt')
for c in np.arange(1.0, 11.0, 0.125):
    clf = svm.SVC(random_state=0, C=c, gamma=0.001, kernel='rbf')
    clf.fit(train_hist, train_label)
    predict = clf.predict(test_hist)
    accuracy_result.write("C =" + str(c) + ", Accuracy:" + str(np.mean(predict == test_label) * 100) + " %" + "\n")
    accuracy_result.write(classification_report(test_label, predict, target_names=get_label()))
    print ("C =", c, ",\t Accuracy:", np.mean(predict == test_label) * 100, "%")
    print(classification_report(test_label, predict, target_names=get_label()))
accuracy_result.close()
# svm_classifier(train_hist, train_label, test_hist, test_label)