import cv2
import numpy as np
import math
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
import copy

from utils import extract_surf_descriptors


def clusterFeatures(all_train_desc, k):
    kmeans = KMeans(n_clusters=k, random_state=0).fit(all_train_desc)
    return kmeans


def extract_denseSURF(img):
    DSURF_STEP_SIZE = 2
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    surf = cv2.xfeatures2d.SURF_create()
    disurf_step_size = DSURF_STEP_SIZE
    keypoints = [cv2.KeyPoint(x, y, disurf_step_size)
                 for y in range(0, gray.shape[0], disurf_step_size)
                 for x in range(0, gray.shape[1], disurf_step_size)]

    descriptors = surf.compute(img, keypoints)[1]

    # keypoints, descriptors = sift.detectAndCompute(gray, None)
    return descriptors


# form histogram with Spatial Pyramid Matching upto level L with codebook kmeans and k codewords
def getImageFeaturesSPM(L, img, kmeans, k):
    W = img.shape[1]
    H = img.shape[0]
    h = []
    for l in range(L + 1):
        w_step = math.floor(W // (2 ** l))
        h_step = math.floor(H // (2 ** l))
        w_step = int(w_step)
        h_step = int(h_step)
        # print w_step, h_step
        x, y = 0, 0
        for i in range(1, 2 ** l + 1):
            x = 0
            for j in range(1, 2 ** l + 1):
                desc = extract_denseSURF(img[y:y + h_step, x:x + w_step])
                # desc = extract_surf_descriptors(img[y:y + h_step, x:x + w_step])
                # print("type:",desc is None, "x:",x,"y:",y, "desc_size:",desc is None)
                predict = kmeans.predict(desc)
                histo = np.bincount(predict, minlength=k).reshape(1, -1).ravel()
                weight = 2 ** (l - L)
                h.append(weight * histo)
                x = x + w_step
            y = y + h_step

    hist = np.array(h).ravel()
    # normalize hist
    dev = np.std(hist)
    hist -= np.mean(hist)
    hist /= dev
    return hist


# get histogram representation for training/testing data
def getHistogramSPM(L, data, kmeans, k):
    x = []
    for i in range(len(data)):
        hist = getImageFeaturesSPM(L, data[i], kmeans, k)
        x.append(hist)
    return np.array(x)


# train model
def trainKNN(data, labels, k):
    neigh = KNeighborsClassifier(n_neighbors=k, p=2)
    neigh.fit(data, labels)
    return neigh


# %%
# form training set histograms for each training image using BoW representation
def formTrainingSetHistogram(x_train, kmeans, k):
    train_hist = []
    for i in range(len(x_train)):
        data = copy.deepcopy(x_train[i])
        predict = kmeans.predict(data)
        train_hist.append(np.bincount(predict, minlength=k).reshape(1, -1).ravel())

    return np.array(train_hist)


# build histograms for test set and predict
def predictKMeans(kmeans, scaler, x_test, train_hist, train_label, test_label, k):
    # form histograms for test set as test data
    test_hist = formTrainingSetHistogram(x_test, kmeans, k)

    # make testing histograms zero mean and unit variance
    test_hist = scaler.transform(test_hist)

    # Train model using KNN
    knn = trainKNN(train_hist, train_label, k)
    predict = knn.predict(test_hist)
    return np.array([predict], dtype=np.array([test_label]).dtype)


def predictTrainKMeans(kmeans, scaler, x_test, train_hist, train_label, k):
    # form histograms for test set as test data
    test_hist = formTrainingSetHistogram(x_test, kmeans, k)

    # make testing histograms zero mean and unit variance
    test_hist = scaler.transform(test_hist)

    # Train model using KNN
    knn = trainKNN(train_hist, train_label, k)
    predict = knn.predict(test_hist)
    return np.array([predict], dtype=np.array([train_label]).dtype)


def accuracy(predict_label, test_label):
    return np.mean(np.array(predict_label.tolist()[0]) == np.array(test_label))


def trainAccuracy(predict_label, train_label):
    return np.mean(np.array(predict_label.tolist()[0]) == np.array(train_label))
